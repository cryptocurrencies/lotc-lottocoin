# ---- Base Node ---- #
FROM ubuntu:14.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libqrencode-dev libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config nano curl cmake && \
    apt-get upgrade -y && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
# Checkout latest Sourcecode
RUN git clone https://github.com/lottocoin/lottocoin.git /opt/lottocoin
# Build All the Dependencies from the build folder
RUN cd /opt/lottocoin/src/ && \
    rm obj && \
    mkdir /opt/lottocoin/src/obj && \
    make -f makefile.unix

# ---- Release ---- #
FROM ubuntu:14.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libqrencode-dev libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/ /tmp/ /var/tmp/*
RUN groupadd -r lottocoin && useradd -r -m -g lottocoin lottocoin
RUN mkdir /data
RUN chown lottocoin:lottocoin /data
COPY --from=build /opt/lottocoin/src/lottocoind /usr/local/bin/lottocoind
USER lottocoin
VOLUME /data
EXPOSE 16384 16383
CMD ["/usr/local/bin/lottocoind", "-datadir=/data", "-conf=/data/lottocoin.conf", "-server", "-txindex", "-printtoconsole" ]
