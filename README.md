This Dockerfile generates the latest Lottocoind client used in server applications. It is anticiapted that you would run the following to install it on your server:

Replacing /localdata with a location on your server where you want to keep the persistant data:
```sh
docker run -d -P --name lottocoin -v /localdata:/data \
registry.gitlab.com/cryptocurrencies/lotc-lottocoin:latest
```